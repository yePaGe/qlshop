//app.js
App({
  onLaunch: function () {
    this.globalData = {
      // env: 'pageye-server-prod-7dlc5cce7ef17', // 正式环境
      env: 'pageye-server-1g4ay6gr0113942c' // 测试环境
    }
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        env: this.globalData.env,
        traceUser: true,
      })
    }
    wx.getSystemInfo({ // iphonex底部适配
      success: res => {
        this.globalData.statusBarHeight = res.statusBarHeight
      }
    })
  }
})
