// pages/mine/mine.js
let app = getApp()
Page({
  data: {
    statusBarHeight: 80,
    avatarUrl: '../../images/account.png',
    nickName: '小可爱'
  },
  onLoad: function () {
    if (app.globalData.statusBarHeight) {
      this.setData({
        statusBarHeight: app.globalData.statusBarHeight
      })
    }
    if (app.globalData.userInfo) {
      this.setData({
        nickName: app.globalData.userInfo.nickName,
        avatarUrl: app.globalData.userInfo.avatarUrl,
      })
    }
  },
  backHome () {
    wx.navigateBack({
      delta: 1,
    })
  },
  checkWage () {
    wx.navigateTo({
      url: '/pages/wage/wage'
    })
  },
  logout () {
    wx.showModal({
      title: '提示',
      content: '确定退出当前账号？',
      success: res => {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: 'userMsg',
            data: {
              type: 'pwd',
              sub_type: 'logout'
            }
          }).then(res => {
            wx.redirectTo({
              url: '/pages/enter/enter'
            })
            app.globalData.role = ''
          })
        }
      }
    })
  }
})