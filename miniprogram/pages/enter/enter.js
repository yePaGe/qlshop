// pages/enter/enter.js
const app = getApp()
Page({
  data: {
    isAuth: false,
    showPwd: false,
    pwd: ''
  },
  onLoad: function (opt) {
    console.log('sss', opt)
    if (opt.autoSign === 'sign') { // 二维码进入自动打卡
      wx.setStorageSync('autoSign', 'sign')
    }
    this.getUserMsg()
  },
  getUserMsg () {
    wx.cloud.callFunction({
      name: 'userMsg',
      data: {
        type: 'get'
      },
      success: res => {
        if (res.result.code === 10000) {
          app.globalData.userInfo = res.result.data
          app.globalData.role = res.result.data.log_role
          this.setData({
            isAuth: true
          })
          console.log('godata', app.globalData);
          if (res.result.data.log_role) {
            this.checkPage(res.result.data)
          }
        }
      }
    })
  },
  checkPage (data) {
    if (data && data.log_role === 'BOSS') {
      wx.redirectTo({
        url: '/pages/boss/boss'
      })
    } else if (data && data.log_role === 'EMPLOYEE') {
      wx.redirectTo({
        url: '/pages/home/home'
      })
    }
  },
  initUserMsg (e) {
    console.log('e', e)
    wx.getUserProfile({
      desc: '请授权您的头像、昵称信息',
      success: msg => {
        wx.cloud.callFunction({
          name: 'userMsg',
          data: {
            type: 'init',
            userInfo: msg.userInfo,
            role: app.globalData.role
          },
          success: res => {
            if (res.result.code === 10000) {
              app.globalData.isAuth = true
              app.globalData.userInfo = res.result.data
              app.globalData.role = res.result.data.log_role
              console.log('global', app.globalData)
            }
          },
          fail: err => {
            console.log('initMsgfail', err)
          }
        })
      }
    })
  },
  changePage (e) {
    let page = e.currentTarget.dataset.page
    wx.redirectTo({
      url: `/pages/${page}/${page}`
    })
  },
  enterPwd () {
    this.setData({
      showPwd: !this.data.showPwd
    })
    if (app.globalData.userInfo) {
      this.setData({
        isAuth: app.globalData.isAuth
      })
    }
  },
  pwdInput (e) {
    let pwd = e.detail.value
    this.setData({
      pwd: pwd
    })
  },
  login () {
    if (!this.data.pwd) {
      wx.showToast({
        title: '请输入登录密码',
        icon: 'error'
      })
      return
    }
    wx.cloud.callFunction({
      name: 'userMsg',
      data: {
        type: 'pwd',
        sub_type: 'login',
        password: this.data.pwd
      }
    }).then(res => {
      if (res.result.code === 10000) {
        wx.redirectTo({
          url: '/pages/boss/boss'
        })
      } else if (res.result.code === 40002) {
        wx.showToast({
          title: '登录密码错误！',
          icon: 'error'
        })
      } else {
        console.log('log_res', res.result.msg)
        wx.showToast({
          title: '登录失败！',
          icon: 'error'
        })
      }
    })
  }
})