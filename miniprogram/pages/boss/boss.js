// pages/boss/boss.js
let app = getApp()
Page({
  data: {
    statusBarHeight: 80,
    isAuth: false,
    avatarUrl: '../../images/account.png',
    nickName: '小可爱',
    tableList: [
      {
        name: '请假审核',
        url: '../../images/rest-audit.png',
        type: 'rest'
      }, {
        name: '员工列表',
        url: '../../images/employee-list.png',
        type: 'employee'
      }, {
        name: '双计日历',
        url: '../../images/calendar.png',
        type: 'calendar'
      }
    ],
    empList: [
      {name: 'sdf'},
      {name: 'sdf'},
      {name: 'sdf'},

    ],
    currentTab: 'table',
    calYear: [],
    currentYear: '2021',
    currentCalendar: 'month',
    calMonth: [],
    currentYearMonth: '2021',
    resYearMonthDate: [],
    bigMonth: [1, 3, 5, 7, 8, 10, 12],
    calDateBk: [],
    calDate: [],
    currentDate: [] // 选中的日期
  },
  onLoad: function (options) {
    if (app.globalData.userInfo) {
      this.setData({
        nickName: app.globalData.userInfo.nickName,
        avatarUrl: app.globalData.userInfo.avatarUrl,
        isAuth: app.globalData.isAuth
      })
    }
    if (app.globalData.statusBarHeight) {
      this.setData({
        statusBarHeight: app.globalData.statusBarHeight
      })
    }
    let yearKey = new Date().getFullYear()
    let year = [...new Array(3)].map((i, k) => (k + yearKey).toString())
    let month = [...new Array(12)].map((i, k) => (k + 1).toString())
    let date = [...new Array(30)].map((i, k) => (k + 1).toString())
    this.setData({
      currentYear: yearKey.toString(),
      calYear: year,
      calMonth: month,
      calDateBk: date
    })
  },
  checkTable (e) {
    let key = e.currentTarget.dataset.item
    if (key === 'rest') {
      wx.navigateTo({
        url: '/pages/rest/rest?type=audit'
      })
    } else {
      this.setData({
        currentTab: key
      })
      if (key === 'employee') {
        this.getEmpList()
      }
    }
  },
  backStep (e) {
    let key = e.currentTarget.dataset.item
    if (key === 'emp' || (key === 'cal' && this.data.currentCalendar === 'month')) {
      this.setData({
        currentTab: 'table'
      })
    } else {
      this.setData({
        currentCalendar: 'month'
      })
    }
  },
  getEmpList () {
    wx.cloud.callFunction({
      name: 'userMsg',
      data: {
        type: 'employees'
      }
    }).then(res => {
      this.setData({
        empList: res.result.data
      })
    })
  },
  checkWage (e) {
    let obj = e.currentTarget.dataset.item
    wx.navigateTo({
      url: '/pages/wage/wage?id=' + obj.openid + '&wage=' + obj.wage
    })
  },
  changeKey (e) {
    let key = e.currentTarget.dataset.key
    this.setData({
      currentYear: key
    })
    this.getCalendarList()
  },
  // 获取双计日历
  getCalendarList () {
    wx.cloud.callFunction({
      name: 'wageData',
      data: {
        type: 'calendar',
        sub_type: 'get',
        year: this.data.currentYear
      }
    }).then(res => {
      if (res.result.code === 10000) {
        this.setData({
          resYearMonthDate: res.result.data
        })
      }
    })
  },
  // 设置选中的年月，并计算出该月的日期数组
  checkMonthDate (e) {
    let key = e.currentTarget.dataset.key
    let date = [].concat(this.data.calDateBk)
    if (this.data.bigMonth.indexOf(Number(key)) > -1) {
      date.push('31')
    } else if (key === '2') {
      let full = Number(this.data.currentYear) % 4 === 0
      if (full) {
        date.splice(-1)
      } else {
        date.splice(-2)
      }
    }
    let currentDate = this.data.resYearMonthDate[key - 1]
    date = date.map(i => {
      let obj = {
        date: i,
        marked: false
      }
      if (currentDate.indexOf(i) > -1) {
        obj.marked = true
      }
      return obj
    })
    this.setData({
      currentCalendar: 'date',
      currentYearMonth: this.data.currentYear + '-' + key,
      calDate: date,
      currentDate: currentDate
    })
  },
  chooseDate (e) {
    let key = e.currentTarget.dataset.key
    let currentDate = [].concat(this.data.currentDate)
    if (key.marked) {
      // 取消标记
      currentDate = currentDate.filter(i => i !== key.date)
    } else {
      // 增加标记
      currentDate.push(key.date)
    }
    let calDate = this.data.calDate.map(i => {
      if (i.date === key.date) {
        i.marked = !i.marked
      }
      return i
    })
    this.setData({
      currentDate: currentDate,
      calDate: calDate
    })
  },
  saveDate () {
    let yearMonth = this.data.currentYearMonth.split('-')
    let resYearMonthDate = [].concat(this.data.resYearMonthDate)
    resYearMonthDate[yearMonth[1] - 1] = this.data.currentDate
    wx.cloud.callFunction({
      name: 'wageData',
      data: {
        type: 'calendar',
        sub_type: 'save',
        year: yearMonth[0],
        list: resYearMonthDate
      }
    }).then(res => {
      if (res.result.code === 10000) {
        wx.showToast({
          title: '双计日历保存成功！',
          icon: 'success'
        })
        this.setData({
          currentCalendar: 'month',
          currentDate: [],
          resYearMonthDate: resYearMonthDate
        })
      }
    })
  },
  logout () {
    wx.showModal({
      title: '提示',
      content: '确定退出当前账号？',
      success: res => {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: 'userMsg',
            data: {
              type: 'pwd',
              sub_type: 'logout'
            }
          }).then(res => {
            wx.redirectTo({
              url: '/pages/enter/enter'
            })
            app.globalData.role = ''
          })
        }
      }
    })
  }
})