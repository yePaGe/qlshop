// pages/rest/rest.js
const app = getApp()

Page({
  data: {
    isGuider: true, // true:店员，false：老板
    avatarUrl: '../../images/account.png',
    nickName: '小可爱',
    restList: [],
    pageTotal: 0,
    showSubmitCon: false,
    editId: '',
    startDate: '',
    startCheck: '',
    endDate: '',
    endCheck: '',
    remark: '',
    searchDate: '',
    searchState: 0
  },
  onLoad: function (opt) {
    if (opt.type === 'audit') {
      this.setData({
        isGuider: false,
        searchState: ''
      })
    }
    if (app.globalData.userInfo) {
      this.setData({
        nickName: app.globalData.userInfo.nickName,
        avatarUrl: app.globalData.userInfo.avatarUrl,
      })
    }
    this.getRestList()
    this.getStateList()
  },
  clearSearch () {
    this.setData({
      searchDate: ''
    })
    this.getRestList()
  },
  getStateList () {
    const db = wx.cloud.database()
    db.collection('dict')
    .where({
      name: 'REST_AUDIT_STATE'
    })
    .get()
    .then(res => {
      let list = []
      if (res.data.length > 0) {
        list = res.data[0].list.map(i => {
          i.checked = false
          return i
        })
      }
      this.setData({
        stateList: list
      })
    })
    .catch(err => {
      console.log('err', err);
    })
  },
  changeList (e) {
    this.setData({
      searchState: e.currentTarget.dataset.key
    })
    this.getRestList()
  },
  getRestList () {
    let data = {
      type: 'list',
      search_time: this.data.searchDate,
      state: this.data.searchState,
      pageSize: 5,
      sub_type: 'employee'
    }
    if (!this.data.isGuider) {
      data.sub_type = 'boss'
    }
    wx.cloud.callFunction({
      name: 'restData',
      data: data
    }).then(res => {
      if (res.result.code === 10000) {
        this.setData({
          restList: res.result.data,
          pageTotal: res.result.total
        })
      }
    })
  },
  showReason (e) {
    let id = e.currentTarget.dataset.id
    let list = this.data.restList.map(i => {
      if (i._id === id) {
        i.hideReason = !i.hideReason
      }
      return i
    })
    this.setData({
      restList: list
    })
  },
  deleteRest (e) {
    let id = e.currentTarget.dataset.id
    wx.showModal({
      title: '提示',
      content: '确定撤销此请假记录吗？',
      success: res => {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: 'restData',
            data: {
              type: 'delete',
              id: id
            }
          }).then(res => {
            if (res.result.code === 10000) {
              wx.showToast({
                title: '撤销请假成功！',
                icon: 'success'
              })
              this.getRestList()
            } else {
              wx.showToast({
                title: '撤销请假失败！',
                icon: 'error'
              })
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  editRest (e) {
    let item = e.currentTarget.dataset.item
    this.setData({
      showSubmitCon: true,
      editId: item._id,
      startDate: item.start_time.slice(0, -6),
      startCheck: item.start_time.slice(-5) === '00:00' ? 'forenoon' : 'afternoon',
      endDate: item.end_time.slice(0, -6),
      endCheck: item.end_time.slice(-5) === '12:00' ? 'forenoon' : 'afternoon',
      remark: item.remark
    })
  },
  addApplication () {
    this.setData({
      showSubmitCon: true,
      startDate: '',
      startCheck: '',
      endDate: '',
      endCheck: '',
      remark: '',
      editId: ''
    })
  },
  closeSubmit () {
    this.setData({
      showSubmitCon: false
    })
  },
  bindDateChange (e) {
    let key = e.currentTarget.dataset.key
    this.setData({
      [key + 'Date']: e.detail.value
    })
    if (key === 'search') {
      this.getRestList()
    }
  },
  radioChange (e) {
    let key = e.currentTarget.dataset.key
    this.setData({
      [key + 'Check']: e.detail.value
    })
  },
  remarkChange (e) {
    this.setData({
      remark: e.detail.value
    })
  },
  submitApplication () {
    let start_time = this.data.startDate + ' 00:00:00'
    if (this.data.startCheck === 'afternoon') {
      start_time = start_time.slice(0, -8) + '12:00:00'
    }
    let end_time = this.data.endDate + ' 12:00:00'
    if (this.data.endCheck === 'afternoon') {
      end_time = end_time.slice(0, -8) + '23:59:59'
    }
    let data = {
      start_time: start_time,
      end_time: end_time,
      remark: this.data.remark,
      creator: this.data.nickName,
      creator_url: this.data.avatarUrl
    }
    let reqType = 'add'
    if (this.data.editId) {
      data.id = this.data.editId
      reqType = 'edit'
    }
    wx.cloud.callFunction({
      name: 'restData',
      data: {
        type: reqType,
        ...data
      }
    }).then(res => {
      if (res.result.code === 10000) {
        this.setData({
          showSubmitCon: false
        })
        let toastTl = reqType === 'edit' ? '编辑请假记录成功！' : '新建请假记录成功！'
        console.log(toastTl);
        wx.showToast({
          title: toastTl,
          icon: 'success'
        })
        this.getRestList()
      } else if (res.result.code === 40000) {
        wx.showToast({
          title: '请选择请假日期',
          icon: 'error'
        })
      } else if (res.result.code === 40002) {
        wx.showToast({
          title: '已有请假记录',
          icon: 'error'
        })
      } else if (res.result.code === 40003) {
        wx.showToast({
          title: '请假信息有误',
          icon: 'error'
        })
      } else {
        wx.showToast({
          title: res.result.msg,
          icon: 'error'
        })
      }
    })
  },
  auditRest (e) {
    let id = e.currentTarget.dataset.id
    let state = e.currentTarget.dataset.state
    wx.showModal({
      title: '提示',
      content: `确定审核${state === '1' ? '通过' : '驳回'}这条请假记录吗？`,
      success: res => {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: 'restData',
            data: {
              type: 'audit',
              id: id,
              state: state
            }
          }).then(res => {
            if (res.result.code === 10000) {
              this.getRestList()
            }
          })
        }
      }
    })
  }
})
