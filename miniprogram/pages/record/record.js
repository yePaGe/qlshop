// pages/record/record.js
import {fixedNumber, monthEndDateFormat} from '../../shared/datetimeFormat'
const app = getApp()

Page({
  data: {
    searchKeyArr: [{
      type: 'month',
      name: '月份'
    }, {
      type: 'date',
      name: '日期'
    }],
    search_key: "0",
    searchMonthArr: [],
    search_year_month: '',
    search_obj: {},
    money: 0,
    search_date: '',
    recordList: [],
    id: '',
    create_time_edit: false,
    create_time_start: '',
    create_time_end: '',
    create_time: '',
    on_time: '',
    off_time: '',
    state: '',
    pageTotal: 0,
    showSubmitCon: false
  },
  onLoad: function () {
    let historyMonth = ['2019-01', '2019-02', '2019-03', '2019-04', '2019-05', '2019-06', '2019-07', '2019-08', '2019-09', '2019-10', '2019-11', '2019-12']
    let monthKey = new Date().getMonth() + 1
    let month = [...new Array(monthKey)].map((i, k) => '2021-' + fixedNumber(k + 1))
    month = historyMonth.concat(month)
    this.setData({                                                        
      searchMonthArr: month,
      search_year_month: month.length - 1,
      create_time_start: monthKey + '-01',
      create_time_end: monthEndDateFormat(monthKey + '-01'),
      search_obj: {
        year_month: month[month.length - 1]
      }
    })
    this.getRecords()
  },
  onPullDownRefresh: function () {
    this.getRecords()
    wx.stopPullDownRefresh()
  },
  searchKeyChange (e) {
    let search = true
    let search_obj = {
      create_time: this.data.search_date
    }
    if (!search_obj.create_time) search = false
    if (e.detail.value === 0) {
      search_obj = {
        year_month: this.data.searchMonthArr[this.data.search_year_month]
      }
      if (!search_obj.year_month) search = false
    }
    this.setData({
      search_key: e.detail.value,
      search_obj: search_obj
    })
    if (search) {
      this.getRecords()
    }
  },
  bindYearMonthChange (e) {
    this.setData({
      search_year_month: e.detail.value,
      search_obj: {
        year_month: this.data.searchMonthArr[e.detail.value]
      }
    })
    this.getRecords(e.detail.value)
  },
  bindDateChange (e) {
    this.setData({
      search_date: e.detail.value,
      search_obj: {
        create_time: e.detail.value
      }
    })
    this.getRecords(e.detail.value)
  },
  clearSearch () {
    this.setData({
      search_date: ''
    })
    this.getRecords()
  },
  getRecords () {
    wx.cloud.callFunction({
      name: 'signupData',
      data: {
        type: 'list',
        wage: app.globalData.userInfo ? app.globalData.userInfo.wage : 11,
        ...this.data.search_obj
      }
    }).then(res => {
      if (res.result.code === 10000) {
        this.setData({
          recordList: res.result.data,
          pageTotal: res.result.total
        })
      }
    })
  },
  editMsg (e) {
    let obj = e.currentTarget.dataset.item
    this.setData({
      showSubmitCon: true,
      id: obj._id,
      create_time: obj.create_time,
      on_time: obj.on_time ? obj.on_time : '请选择上班时间',
      off_time: obj.off_time ? obj.off_time : '请选择下班时间',
      state: obj.state.type
    })
  },
  addMsg () {
    this.setData({
      showSubmitCon: true,
      create_time_edit: true,
      create_time: '请选择日期',
      on_time: '请选择上班时间',
      off_time: '请选择下班时间'
    })
  },
  bindCreateTimeChange (e) {
    this.setData({
      create_time: e.detail.value
    })
  },
  bindTimeChange (e) {
    let key = e.currentTarget.dataset.key
    this.setData({
      [key + '_time']: e.detail.value
    })
  },
  closeMsg () {
    this.setData({
      showSubmitCon: false,
      id: '',
      create_time: '',
      on_time: '',
      off_time: '',
      state: ''
    })
  },
  submitEdit () {
    if (!this.data.on_time) {
      wx.showToast({
        title: '请选择上班时间',
        icon: 'error'
      })
      return
    }
    if (!this.data.off_time) {
      wx.showToast({
        title: '请选择下班时间',
        icon: 'error'
      })
      return
    }
    let ontime = new Date('2021-01-01 ' + this.data.on_time + ':00').getTime()
    let offtime = new Date('2021-01-01 ' + this.data.off_time + ':00').getTime()
    if (offtime < ontime) {
      wx.showToast({
        title: '下班要晚于上班',
        icon: 'error'
      })
      return
    }
    let obj = {
      type: 'set',
      sub_type: 'supply',
      create_time: this.data.create_time,
      on_time: this.data.on_time + ':00',
      off_time: this.data.off_time + ':00',
      state: this.data.state
    }
    wx.cloud.callFunction({
      name: 'signupData',
      data: obj
    }).then(res => {
      if (res.result.code === 10000) {
        this.getRecords()
        this.closeMsg()
      }
    })
  }
})