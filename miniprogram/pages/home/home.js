// pages/home/home.js
const app = getApp()
import { dateFormat, timeFormat } from '../../shared/datetimeFormat.js'
var currentInterval = null

Page({
  data: {
    avatarUrl: 'https://7061-pageye-server-prod-7dlc5cce7ef17-1304695432.tcb.qcloud.la/account.png?sign=abe8dd420b4a0cab818ac091fa2c620e&t=1612494788',
    nickName: '小可爱',
    on_time: '未打卡',
    off_time: '未打卡',
    currentTime: '00:00:00',
    state: {
      type: 1,
      name: '上班'
    }
  },
  onLoad: function() {
    this.setData({
      currentTime: timeFormat(new Date())
    })
    this.showCurrentTime()
    if (app.globalData.userInfo) {
      this.setData({
        nickName: app.globalData.userInfo.nickName,
        avatarUrl: app.globalData.userInfo.avatarUrl
      })
    }
    this.getSignupMsg()
  },
  onShow: function () {
    if (typeof this.getTabBar === 'function' && this.getTabBar()) {
      this.getTabBar().setData({
        currentTab: 1
      })
    }
  },
  onUnload: function () {
    clearInterval(currentInterval)
    this.setData({
      currentTime: ''
    })
  },
  showCurrentTime () {
    currentInterval = setInterval(() => {
      this.setData({
        currentTime: timeFormat(new Date())
      })
    }, 1000)
  },
  getUserProfile: function (e) {
    wx.getUserProfile({
      desc: '业务需要',
      success: res => {
      	console.log(res)
      }
    })
  },
  changePage (e) {
    const route = e.currentTarget.dataset.route
    wx.navigateTo({
      url: `/pages/${route}/${route}`
    })
  },
  clickSignup () {
    if (this.data.stateCode === 0) return
    wx.showLoading({
      title: '嘀~打卡中'
    })
    wx.cloud.callFunction({
      name: 'signupData',
      data: {
        type: 'set',
        sub_type: 'sign',
        create_time: dateFormat(new Date()),
        state: this.data.state.type,
        sign_time: this.data.currentTime
      }
    }).then(res => {
      wx.hideLoading()
      if (res.result.code === 10000) {
        wx.showToast({
          title: '打卡成功'
        })
        let data = res.result.data
        this.setData({
          on_time: data.on_time ? data.on_time : '未打卡',
          off_time: data.off_time ? data.off_time : '未打卡',
          state: data.state 
        })
      }
    })
  },
  getSignupMsg () {
    wx.cloud.callFunction({
      name: 'signupData',
      data: {
        type: 'get',
        create_time: dateFormat(new Date())
      }
    }).then(res => {
      if (res.result.code === 10000) {
        let data = res.result.data
        this.setData({
          on_time: data.on_time ? data.on_time : '未打卡',
          off_time: data.off_time ? data.off_time : '未打卡',
          state: data.state
        })
      }
      console.log('sign', wx.getStorageSync('autoSign'), wx.getStorageSync('autoSign') === 'sign');
      if (wx.getStorageSync('autoSign') === 'sign') { // 二维码进入自动打卡
        this.clickSignup()
        wx.removeStorageSync('autoSign')
      }
    })
  }
})