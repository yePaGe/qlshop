// pages/mine/mine.js
import {fixedNumber} from '../../shared/datetimeFormat'
const app = getApp()

Page({
  data: {
    openid: '',
    canEdit: false,
    avatarUrl: '',
    nickName: '',
    year: '2021',
    arrayYear: [],
    month: '',
    arrayMonth: [],
    money: 0,
    wageList: [],
    needInit: 0,
    wage: 11,
    newWage: 11,
    showWageEdit: false
  },
  onLoad: function (opt) {
    let monthKey = new Date().getMonth()
    let month = [...new Array(monthKey)].map((i, k) => fixedNumber(k + 1))
    this.setData({
      arrayMonth: month,
      month: fixedNumber(monthKey),
      openid: opt.id,
      canEdit: opt.id ? true : false,
      wage: opt.id ? opt.wage : app.globalData.userInfo.wage ? app.globalData.userInfo.wage : 11,
      avatarUrl: app.globalData.userInfo.avatarUrl,
      nickName: app.globalData.userInfo.nickName
    })
    this.getWageList()
  },
  getWageList () {
    let date_key = this.data.year + '-' + this.data.month
    wx.cloud.callFunction({
      name: 'wageData',
      data: {
        type: 'list',
        year_month: date_key,
        wage: this.data.wage,
        init: this.data.needInit,
        id: this.data.openid
      }
    }).then(res => {
      this.setData({
        wageList: res.result.data,
        money: res.result.total_wage,
        needInit: 0
      })
    })
  },
  bindMonthChange (e) {
    this.setData({
      month: this.data.arrayMonth[e.detail.value]
    })
    this.getWageList()
  },
  initMoneyMsg () {
    this.setData({
      needInit: 1
    })
    this.getWageList()
  },
  showWageEditCon () {
    this.setData({
      showWageEdit: true,
      newWage: this.data.wage
    })
  },
  saveWageEdit () {

  },
  cancelWageEdit () {
    this.setData({
      showWageEdit: false
    })
  }
})