/**
 * 格式化日期
 * @param {Date} date
 * @return {String} 返回日期字符串
 */
export const dateFormat = function (date) {
  if (typeof date === 'object') {
    let year = date.getFullYear()
    let month = fixedNumber(date.getMonth() + 1)
    let day = fixedNumber(date.getDate())
    return `${year}-${month}-${day}`
  } else {
    console.log('日期无效')
  }
}

/**
 * 格式化时间
 * @param {Date} date
 * @return {String} 返回时间字符串
 */
export const timeFormat = function (date) {
  if (typeof date === 'object') {
    let hour = date.getHours()
    let minutes = fixedNumber(date.getMinutes())
    let seconds = fixedNumber(date.getSeconds())
    return `${hour}:${minutes}:${seconds}`
  } else {
    console.log('日期无效')
    return ''
  }
}

/**
 * 格式化日期时间
 * @param {Date} date
 * @return {String} 返回日期时间字符串
 */
export const dateTimeFormat = function (date) {
  if (typeof date === 'object') {
    let year = date.getFullYear()
    let month = fixedNumber(date.getMonth() + 1)
    let day = fixedNumber(date.getDate())
    let hour = date.getHours()
    let minutes = fixedNumber(date.getMinutes())
    let seconds = fixedNumber(date.getSeconds())
    return `${year}-${month}-${day} ${hour}:${minutes}:${seconds}`
  } else {
    console.log('日期无效')
    return ''
  }
}

/**
 * 获取当前日期的月尾时间
 * @param {String} date 月头日期字符串
 * @return {String} 返回日期字符串
 */
export const monthEndDateFormat = function (date) {
  let startDate = new Date(date + ' 00:00:00')
  let startYear = startDate.getFullYear()
  let startMonth = fixedNumber(startDate.getMonth() + 2)
  let endDate = new Date(new Date(startYear + '-' + startMonth + '-01 00:00:00').getTime() -1000)
  let endYear = endDate.getFullYear()
  let endMonth = fixedNumber(endDate.getMonth() + 1)
  let endDay = fixedNumber(endDate.getDate())
  return `${endYear}-${endMonth}-${endDay}`
}

/**
 * 数字双位处理
 * @param {Number} number 待处理数字
 * @return {String} 返回双位数字字符串
 */
export const fixedNumber = function (number) {
  return number.toString().length === 1 ? '0' + number.toString() : number.toString()
}
