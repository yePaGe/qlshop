// 云函数入口文件
const cloud = require('wx-server-sdk')
const code = require('./response_code')
const format = require('./format.js')
cloud.init({
  // env: 'pageye-server-prod-7dlc5cce7ef17', // 正式环境
  env: 'pageye-server-1g4ay6gr0113942c' // 测试环境
})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  let reqData = {
    openid: wxContext.OPENID,
    create_time: event.create_time,
    year_month_key: event.year_month
  }
  return db.collection('signup')
  .where(reqData)
  .get()
  .then(res => {
    return db.collection('dict')
      .where({name: 'SIGNUP_STATE'})
      .get()
      .then(dict => {
        if (event.type === 'set') { // 增加或更新打卡记录
          Object.assign(event, {openid: wxContext.OPENID})
          return recordEdit(event, res.data[0], dict)
        } else { // 获取打卡记录
            // get: 获取单个打卡记录
            // list：打卡记录列表
          if (event.type === 'get' && res.data.length === 0) {
            return {
              ...code[40003],
              sub_msg: 'signup data is blank'
            }
          }
          let resData = res.data.map(i => {
            i.state = dict.data[0].list[i.state]
            if (i.on_time) i.on_time = i.on_time.slice(0, -3)
            if (i.off_time) i.off_time = i.off_time.slice(0, -3)
            return i
          })
          resData = format.listSort(resData)
          if (event.type === 'get') resData = resData[0]
          return {
            ...code[10000],
            data: resData,
            total: resData.length
          }
        }
      })
  })
}

function recordEdit(reqData, resData, dict) {
  if (resData && resData._id) { // 已有记录，更新记录
    let id = resData._id
    delete(resData._id)
    if (reqData.sub_type === 'sign') { // 打卡
      if (reqData.state === 1) { // 上班状态
        resData.on_time = reqData.sign_time
        resData.state = 2
      }
      if (reqData.state === 2) { // 下班状态
        resData.off_time = reqData.sign_time
        resData.state = 0
      }
      if (resData.state === 0) { // 更新状态
        resData.off_time = reqData.sign_time
      }
      if (resData.on_time && resData.off_time) resData.state = 0 // 已有上下班打卡记录，更改为更新状态
    } else if (reqData.sub_type === 'supply') { // 补卡
      resData.on_time = reqData.on_time
      resData.off_time = reqData.off_time
      resData.state = 0
    }
    return db.collection('signup')
    .doc(id)
    .set({ data: resData })
    .then(() => {
      resData.state = dict.data[0].list[resData.state]
      return {
        ...code[10000],
        data: resData
      }
    })
    .catch(err => {
      console.log('signup_set_update_err: ', err)
      return {
        ...code[20000]
      }
    })
  } else { // 创建新记录
    if (reqData.sub_type === 'sign') { // 打卡
      resData = {
        openid: reqData.openid,
        create_time: reqData.create_time,
        year_month_key: reqData.create_time.slice(0, -3),
        on_time: '',
        off_time: '',
        state: 1 // 默认上班打卡状态
      }
      if (reqData.state === 1) {
        resData.on_time = reqData.sign_time
        resData.state = 2 // 打卡上班记录后，状态改为下班打卡状态
      }
    } else if (reqData.sub_type === 'supply') { // 补卡
      resData = {
        openid: reqData.openid,
        create_time: reqData.create_time,
        year_month_key: reqData.create_time.slice(0, -3),
        on_time: reqData.on_time,
        off_time: reqData.off_time,
        state: 0 // 默认上班打卡状态
      }
    }
    return db.collection('signup')
    .add({ data: resData })
    .then(() => {
      resData.state = dict.data[0].list[resData.state]
      return {
        ...code[10000],
        data: resData
      }
    })
    .catch(err => {
      console.log('signup_set_add_err: ', err)
      return {
        ...code[20000]
      }
    })
  }
}
