// 云函数入口文件
const cloud = require('wx-server-sdk')
const hoursDiff = require('./hoursDiff.js')
cloud.init({
  env: 'pageye-server-1g4ay6gr0113942c'
})
const db = cloud.database({
  env: 'pageye-server-1g4ay6gr0113942c'
})
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  switch (event.type) {
    case 'get':
      return dataGet({
          openid: wxContext.OPENID,
          create_time: event.create_time
        },
        res => {
          if (res.data.length > 0) {
            return {
              code: 10000,
              msg: 'success',
              data: res.data[0]
            }
          } else {
            return {
              code: 10001,
              msg: 'signup data blank'
            }
          }
        },
        () => {
          return {
            code: 20000,
            msg: 'wrong server'
          }
        }
      )
    case 'set':
      return dataGet({
          openid: wxContext.OPENID,
          create_time: event.create_time
        },
        res => {
          return dataSet(wxContext.OPENID, res.data[0], event)
        },
        () => {
          console.log('datageterr', err)
          return {
            code: 20000,
            msg: 'wrong server'
          }
        }
      )
    case 'list':
      let listData = {
        openid: wxContext.OPENID
      }
      if (event.create_time) {
        listData.create_time = event.create_time
      }
      return dataGet(listData,
        res => {
          console.log('sigres', res.data);
          let obj = calWage(res.data, event.wage)
          return {
            code: 10000,
            msg: 'success',
            data: obj.list,
            total: obj.list.length,
            total_wage: obj.wage
          }
        },
        () => {
          return {
            code: 20000,
            msg: 'wrong server'
          }
        }
      )
    case 'total':
      return dataTotal(wxContext.OPENID, event)
    case '':
      
  }
}

function dataGet (data, success, fail) {
  return db.collection('signup-list')
  .where(data)
  .orderBy('create_time', 'desc')
  .get()
  .then(res => {
    return success(res)
  })
  .catch(err => {
    console.log('datageterr', err)
    return fail(err)
  })
}
function dataSet (id, data, req) {
  let reqData = {
    openid: id,
    create_time: req.create_time,
    on_time: '',
    off_time: '',
    state: '',
    date_key: '',
    wage: 11
  }
  if (data && data.create_time && data.create_time === req.create_time) {
    reqData.on_time = data.on_time
    reqData.off_time = data.off_time
    reqData.date_key = data.create_time.slice(0, -3)
  }
  if (req.on_time && req.off_time) {
    reqData.on_time = req.on_time
    reqData.off_time = req.off_time
  }
  if (req.stateCode === 1) {
    reqData.on_time = req.signTime
    reqData.state = 2
  } else if (req.stateCode === 2) {
    reqData.off_time = req.signTime
    reqData.state = 0
  } else {
    reqData.state = 0
  }
  if (data && data._id) {
    return db.collection('signup-list')
    .doc(data._id)
    .set({ data: reqData })
    .then(() => {
      return db.collection('dict-list').where({
        name: 'REST_STATE'
      }).get().then(dict => {
        reqData.state = dict.data[0].list[reqData.state]
        return {
          code: 10000,
          msg: 'success',
          data: reqData
        }
      })
    })
    .catch(() => {
      return {
        code: 20000,
        msg: 'wrong server'
      }
    })
  } else { // 新增打卡记录
    return db.collection('signup-list')
    .add({ data: reqData })
    .then(() => {
      return {
        code: 10000,
        msg: 'success',
        data: reqData
      }
    })
    .catch(() => {
      return {
        code: 20000,
        msg: 'wrong server'
      }
    })
  }
}

function dataTotal (openid, reqData) {
  let totalData = {
    openid: openid,
    date_key: reqData.date_key
  }
  if(reqData.id) {
    totalData.openid = reqData.id
  }
  return dataGet(totalData,
    signRes => {
      let signupList = signRes.data
      return  restDataList(reqData.date_key, res => {
        // 筛选出当月请假记录
        let startNum = res.result.time.start
        let endNum = res.result.time.end
        let restDay = []
        let upmidDay = []
        let downmidDay = []
        let bk = []
        // 整理出请假日期，并区别出全天假或上半天假或下半天假
        res.result.data.forEach(i => {
          // 请假开始时间早于当月1号，结束时间早于当月尾号ok
          if (i.start_time_stamp < startNum 
            && i.end_time_stamp > startNum 
            && i.end_time_stamp < endNum) {
            let endKey = Number(i.end_time.split(' ')[0].slice(-2))
            if (new Date(i.end_time_stamp).getHours() === 12) {
              upmidDay.push(endKey)
              endKey--
            }
            for (let d = 1; d <= endKey; d++) {
              restDay.push(d)
            }
          }
          // 请假结束时间晚于当月尾号，开始时间晚于当月1号
          if (i.start_time_stamp > startNum
            && i.start_time_stamp < endNum 
            && i.end_time_stamp > endNum) {
            let startKey = new Date(i.start_time_stamp).getDate()
            if (new Date(i.start_time_stamp).getHours() === 12) {
              downmidDay.push(startKey)
              startKey++
            }
            let endKey = new Date(endNum).getDate()
            for (let d = startKey; d <= endKey; d++) {
              restDay.push(d)
            }
          }
          // 请假时间范围在当月范围内ok
          if (i.start_time_stamp > startNum
            && i.end_time_stamp < endNum) {
          let startKey = new Date(i.start_time_stamp).getDate()
            if (new Date(i.start_time_stamp).getHours() === 12) {
              downmidDay.push(startKey)
              startKey++
            }
            let endKey = new Date(i.end_time_stamp).getDate()
            if (new Date(i.end_time_stamp).getHours() === 12) {
              upmidDay.push(endKey)
              endKey--
            }
            for (let d = startKey; d <= endKey; d++) {
              restDay.push(d)
            }
          }
        })
        // 根据整理出来的日期，标记全天假日期
        if (restDay.length > 0) {
          signupList = signupList.map(i => {
            let day = Number(i.create_time.slice(-2))
            if (restDay.indexOf(day) > -1) {
              i.rest = 3 // 全天
            }
            return i
          })
        }
        // 根据整理出来的日期，标记上半天假日期
        if (upmidDay.length > 0) {
          signupList = signupList.map(i => {
            let day = Number(i.create_time.slice(-2))
            if (upmidDay.indexOf(day) > -1) {
              i.rest = 1 // 上半天
            }
            return i
          })
        }
        // 根据整理出来的日期，标记下半天假日期
        if (downmidDay.length > 0) {
          signupList = signupList.map(i => {
            let day = Number(i.create_time.slice(-2))
            if (downmidDay.indexOf(day) > -1) {
              i.rest = 2 // 下半天
            }
            return i
          })
        }
        // 计算工资
        let wageObj = calWage(signupList, reqData.wage)
        return {
          code: 10000,
          msg: 'success',
          data: wageObj.list,
          total_wage: wageObj.wage
        }
      }, err => {
        console.log('restotalerr', err);
      })
    },
    () => {
      return {
        code: 20000,
        msg: 'wrong server'
      }
    }
  )
}

function restDataList (dateKey, success, fail) {
  return cloud.callFunction({
    name: 'restData',
    data: {
      type: 'list',
      state: 1,
      req_type: 1,
      search_key: dateKey,
      pageSize: 5
    }
  }).then(res => {
    console.log('list', res)
    if (res.result.code === 10000) {
      return success(res)
    } else {
      return fail(res)
    }
  })
}

function calWage (list, wageKey) {
  let wage = 0
  list = list.map(i => {
    if (i.on_time && i.off_time) {
      let startNumber = new Date(i.create_time + ' ' + i.on_time)
      let endNumber = new Date(i.create_time + ' ' + i.off_time)
      let offNumber = new Date(i.create_time + ' 21:00:00')
      i.date = i.create_time.slice(-2)
      i.on_time = i.on_time.slice(0, -3)
      i.off_time = i.off_time.slice(0, -3)
      if (i.rest === 1) {
        endNumber = new Date(i.create_time + ' 12:00:00')
      }
      if (i.rest === 2) {
        startNumber = new Date(i.create_time + ' 12:00:00')
      }
      console.log('hours', startNumber, endNumber);
      i.hours = hoursDiff.diff(startNumber, endNumber, 1)
      i.meal = 1
      i.lunch = 1
      if (endNumber > offNumber && i.rest !== 2) { // 九点之后下班且下午没请假，加晚餐费
        i.meal++
        i.dinner = 1
      }
      if (i.rest === 1) { // 上午请假了，没午餐费
        i.meal--
        i.lunch = 0
      }
      if (i.rest === 2 && i.dinner === 1) { // 下午请假了，没晚餐费
        i.meal--
        i.dinner = 0
      }
      i.pay = Number((i.hours * wageKey + i.meal * 10).toFixed(2))
      if (i.rest === 3) {
        i.pay = 0
      }
      wage = Number((wage + i.pay).toFixed(2))
    } else {
      if (!i.on_time) i.on_time = '未打卡'
      else i.on_time = i.on_time.slice(0, -3)
      if (!i.off_time) i.off_time = '未打卡'
      else i.off_time = i.off_time.slice(0, -3)
      i.hours = 0
      i.lunch = 0
      i.dinner = 0
      i.pay = 0
      i.meal = 0
    }
    return i
  })
  
  return {
    list,
    wage
  }
}
