/**
 * 对象数组日期排序
 * @param {Array} list 列表
 * @return {Array} 返回类型：排序后的列表
 */
exports.listSort = function (list) {
  list.sort(compare)
  return list
}

function compare (obj1, obj2) {
  let date1 = new Date(obj1.create_time + ' 00:00:00').getTime()
  let date2 = new Date(obj2.create_time + ' 00:00:00').getTime()
  if (Number(date1) < Number(date2)) {
    return -1
  } else if (Number(date1) > Number(date2)) {
    return 1
  } else {
    return 0
  }
}