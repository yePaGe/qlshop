// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'pageye-server-1g4ay6gr0113942c'
})
const db = cloud.database({
  env: 'pageye-server-1g4ay6gr0113942c'
})
const _ = db.command

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  switch (event.type) {
    case 'set':
      return restDataSet(wxContext.OPENID, event)
    case 'edit':
      return restDataEdit(event)
    case 'delete':
      return restDataDelete(event)
    case 'list':
      return restDataList(wxContext.OPENID, event)
    case 'audit':
      return restDataAudit(event)
  }
}

function restDataGet (openid, reqData, success, fail) {
  if (!reqData.start_time || !reqData.end_time) {
    return fail(40000)
  }
  let startNum = new Date(reqData.start_time).getTime()
  let endNum = new Date(reqData.end_time).getTime()
  if (isNaN(startNum) || isNaN(endNum)) {
    return fail(40003)
  }
  return db.collection('rest-list')
  .where(_.and([
    _.and({
      openid: openid
    }),
    _.or([
      { // 1、在已有记录范围内
        start_time_stamp: _.lte(startNum),
        end_time_stamp: _.gte(endNum)
      },
      { // 2、跨越开始时间，不接触结束时间
        start_time_stamp: _.and([_.gte(startNum), _.lte(endNum)])
      },
      { // 3、跨越结束时间，不接触开始时间
        end_time_stamp: _.and([_.gte(startNum), _.lte(endNum)])
      },
      { // 4、跨越开始时间和结束时间
        start_time_stamp: _.gte(startNum),
        end_time_stamp: _.lte(endNum)
      }
    ])
  ]))
  .get()
  .then(res => {
    console.log('abc', res);
    return success(res, {
      start_time_stamp: startNum,
      end_time_stamp: endNum
    })
  })
  .catch(err => {
    return fail(err)
  })
}

function restDataSet (openid, reqData) {
  return restDataGet(openid, reqData, (res, time) => {
    if (res.data.length > 0) {
      return {
        code: 40002,
        msg: 'record existed'
      }
    } else {
      let data = {
        openid: openid,
        start_time: reqData.start_time,
        start_time_stamp: time.start_time_stamp,
        end_time: reqData.end_time,
        end_time_stamp: time.end_time_stamp,
        remark: reqData.remark ? reqData.remark : '-',
        state: 0
      }
      return db.collection('rest-list')
      .add({data})
      .then(() => {
        return {
          code: 10000,
          msg: 'success'
        }
      })
      .catch(() => {
        return {
          code: 20000,
          msg: 'wrong server'
        }
      })
    }
  }, err => {
    if (err === 40000) { // 缺少请求参数
      return {
        code: 40000,
        msg: 'request data missing'
      }
    } else if (err === 40003) { // 请求参数有误
      return {
        code: 40003,
        msg: 'request data wrong'
      }
    } else {
      return {
        code: 20000,
        msg: 'wrong server'
      }
    }
  })
}

function restDataList (openid, reqData) {
  let data = {
    openid: openid,
    state: reqData.state
  }
  let startNum = 0
  let endNum = 0
  // 用于请假记录的筛选
  if (reqData.search_time) { // 筛选出开始和结束时间在搜索日期的0点到24点之间的数据
    startNum = new Date(reqData.search_time + ' 00:00:00').getTime()
    endNum = new Date(reqData.search_time + ' 23:59:59').getTime()
    data.start_time_stamp = _.lte(startNum)
    data.end_time_stamp = _.gte(endNum)
  }
  // 用于工资计算的筛选
  if (reqData.req_type === 1 && reqData.search_key) {
    let endKey = reqData.search_key.split('-')
    startNum = new Date(reqData.search_key + '-01 00:00:00').getTime()
    endNum = 0
    if (Number(endKey[1] + 1) > 12) {
      endNum = new Date(Number(endKey[0])+1 + '-01-01 00:00:00').getTime() - 1000
    } else {
      endNum = new Date(endKey[0] + '-' + ((Number(endKey[1]) + 1) < 10 ? '0' + (Number(endKey[1]) + 1) : (Number(endKey[1]) + 1)) + '-01 00:00:00').getTime() - 1000
    }
    data = _.and([
      _.and({
        openid: openid
      }),
      _.or([
        { // 1、在搜索范围内
          start_time_stamp: _.and([_.gte(startNum), _.lte(endNum)]),
          end_time_stamp: _.and([_.gte(startNum), _.lte(endNum)])
        },
        { // 2、跨越开始时间，不接触结束时间
          start_time_stamp: _.lte(startNum),
          end_time_stamp: _.and([_.gte(startNum), _.lte(endNum)])
        },
        { // 3、跨越结束时间，不接触开始时间
          start_time_stamp: _.and([_.gte(startNum), _.lte(endNum)]),
          end_time_stamp: _.gte(endNum)
        },
        { // 4、跨越开始时间和结束时间
          start_time_stamp: _.lte(startNum),
          end_time_stamp: _.gte(endNum)
        }
      ])
    ])
  }
  return db.collection('rest-list')
  .where(data)
  .orderBy('start_time_stamp', 'asc')
  .limit(reqData.pageSize)
  .get()
  .then(res => {
    console.log('restlist', res);
    if (reqData.req_type === 1) { // 工资计算查询
      return {
        code: 10000,
        msg: 'success',
        data: res.data,
        time: {
          start: startNum,
          end: endNum
        }
      }
    } else {
      return db.collection('dict-list').where({
        name: 'REST_STATE'
      }).get().then(dict => {
        let list = res.data.map(i => {
          return {
            _id: i._id,
            start_time: i.start_time.slice(0, -3),
            end_time: i.end_time.slice(0, -3),
            state: dict.data[0].list[i.state],
            remark: i.remark,
            showBtn: i.remark.replace(/[\u0391-\uFFE5]/g,"aa").length > 20 ? true : false,
            hideReason: i.remark.replace(/[\u0391-\uFFE5]/g,"aa").length > 20 ? true : false
          }
        })
        return {
          code: 10000,
          msg: 'success',
          data: list,
          total: res.data.length
        }
      })
    }
  })
  .catch(err => {
    console.log('listerr', err); 
    return {
      code: 20000,
      msg: 'wrong server'
    }
  })
}

function restDataEdit (openid, reqData) {
  return restDataGet(openid, reqData, (res, time) => {
    if (res.data.length === 1) {
      if (res.data._id === reqData.id) { // 如果id是提交的那条请假，则允许修改
        return restDataEditSubmit(openid, reqData, time)
      } else {
        return {
          code: 40002,
          msg: 'record existed'
        }
      }
    } else if (res.data.length > 1) {
      return {
        code: 40002,
        msg: 'record existed'
      }
    } else {
      return restDataEditSubmit(openid, reqData, time)
    }
  }, () => {
    return {
      code: 20000,
      msg: 'wrong server'
    }
  })
}

function restDataEditSubmit (openid, reqData) {
  let data = {
    openid: openid,
    start_time: reqData.start_time,
    start_time_stamp: time.start_time_stamp,
    end_time: reqData.end_time,
    end_time_stamp: time.end_time_stamp,
    state: 0
  }
  return db.collection('rest-list')
  .doc(reqData.id)
  .set(data)
  .then(() => {
    return {
      code: 10000,
      msg: 'success'
    }
  })
  .catch(() => {
    return {
      code: 20000,
      msg: 'wrong server'
    }
  })
}

function restDataDelete (reqData) {
  return db.collection('rest-list')
  .doc(reqData.id)
  .remove()
  .then(res => {
    console.log('delres', res)
    if (res.stats.removed === 1) {
      return {
        code: 10000,
        msg: 'success'
      }
    } else {
      return {
        code: 20000,
        msg: 'wrong server'
      }
    }
  })
}

function restDataAudit (reqData) {
  return db.collection('rest-list')
  .doc(reqData.id)
  .update({
    data: {
      state: Number(reqData.state)
    }
  })
  .then(res => {
    return {
      code: 10000,
      msg: 'success'
    }
  })
  .catch(err => {
    return {
      code: 20000,
      msg: 'wrong server'
    }
  })
}