// 请假记录云函数
const cloud = require('wx-server-sdk')
const code = require('./response_code')
cloud.init({
  // env: 'pageye-server-prod-7dlc5cce7ef17', // 正式环境
  env: 'pageye-server-1g4ay6gr0113942c' // 测试环境
})
const db = cloud.database()
const _ = db.command

exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  Object.assign(event, {openid: wxContext.OPENID})
  switch (event.type) {
    case 'add': // 增加请假记录
      return recordAdd(event)
    case 'edit': // 修改请假记录
    case 'audit': // 审核请假记录
      return recordEdit(event)
    case 'delete':
      return recordDelete(event.id)
    case 'list':
      return recordList(event)
  }
}

function recordAdd (reqData) {
  if (!reqData.start_time || !reqData.end_time) {
    return {
      ...code[40000]
    }
  }
  let startNum = new Date(reqData.start_time).getTime()
  let endNum = new Date(reqData.end_time).getTime()
  if (isNaN(startNum) || isNaN(endNum)) {
    return {
      ...code[40002]
    }
  }
  return db.collection('rest')
  .where(_.and([
    _.and({
      openid: reqData.openid
    }),
    _.or([
      { // 1、在已有记录范围内
        start_time_stamp: _.lte(startNum),
        end_time_stamp: _.gte(endNum)
      },
      { // 2、跨越开始时间，不接触结束时间
        start_time_stamp: _.and([_.gte(startNum), _.lte(endNum)])
      },
      { // 3、跨越结束时间，不接触开始时间
        end_time_stamp: _.and([_.gte(startNum), _.lte(endNum)])
      },
      { // 4、跨越开始时间和结束时间
        start_time_stamp: _.gte(startNum),
        end_time_stamp: _.lte(endNum)
      }
    ])
  ]))
  .get()
  .then(res => {
    if (res.data.length > 0) { // 查到记录，则报错40001
      return {
        ...code[40001]
      }
    } else { // 查无当前记录，则增加记录
      let data = {
        openid: reqData.openid,
        create_time: db.serverDate(),
        last_update_time: db.serverDate(),
        creator: reqData.creator,
        creator_url: reqData.creator_url,
        start_time: reqData.start_time,
        start_time_stamp: startNum,
        end_time: reqData.end_time,
        end_time_stamp: endNum,
        remark: reqData.remark ? reqData.remark : '-',
        state: 0 // 默认待审核状态
      }
      return db.collection('rest')
      .add({data})
      .then(() => {
        return {
          ...code[10000]
        }
      })
      .catch(add_err => {
        console.log('signup_add_err: ', add_err);
        return {
          ...code[20000]
        }
      })
    }
  })
  .catch(err => {
    console.log('signup_get_err: ', err);
    return {
      ...code[20000]
    }
  })
}

function recordEdit (reqData) {
  if (reqData.type === 'edit') { // 修改记录
    reqData.last_update_time = db.serverDate()
    reqData.start_time_stamp = new Date(reqData.start_time).getTime()
    reqData.end_time_stamp = new Date(reqData.end_time).getTime()
    reqData.state = 0 // 状态再次改为待审核状态
    delete(reqData.type)
  } else if (reqData.type === 'audit') { // 审核记录
    reqData = {
      state: Number(reqData.state)
    }
  }
  return db.collection('rest')
  .doc(reqData.id)
  .update({data: reqData})
  .then(() => {
    if (res.stats.updated === 1) {
      return {
        ...code[10000]
      }
    } else {
      return {
        ...code[40004],
        sub_msg: res.errMsg
      }
    }
  })
  .catch(err => {
    console.log('rest_edit_err: ', err)
    return {
      ...code[20000]
    }
  })
}

function recordDelete (reqId) {
  return db.collection('rest')
  .doc(reqData.id)
  .remove()
  .then(() => {
    if (res.stats.removed === 1) {
      return {
        ...code[10000]
      }
    } else {
      return {
        ...code[40004],
        sub_msg: res.errMsg
      }
    }
  })
  .catch(err => {
    console.log('rest_delete_err: ', err)
    return {
      ...code[20000]
    }
  })
}

function recordList (reqData) {
  let data = {
    openid: reqData.openid
  }
  let startNum = 0
  let endNum = 0
  // 用于请假记录的筛选
  if (reqData.sub_type === 'boss') { // 老板角色查询不带id
    delete(data.openid)
  }
  if ([0, 1, 2].indexOf(reqData.state) > -1) { // 查询有状态条件
    data.state = reqData.state
  }
  if (reqData.search_time) { // 筛选出开始和结束时间在搜索日期的0点到24点之间的数据
    startNum = new Date(reqData.search_time + ' 00:00:00').getTime()
    endNum = new Date(reqData.search_time + ' 23:59:59').getTime()
    data.start_time_stamp = _.lte(startNum)
    data.end_time_stamp = _.gte(endNum)
  }
  console.log('data', data);
  return db.collection('rest')
  .where(data)
  .orderBy('start_time_stamp', 'asc')
  .limit(reqData.pageSize)
  .get()
  .then(res => {
    console.log('restlist', res)
    return db.collection('dict')
    .where({name: 'REST_AUDIT_STATE'})
    .get()
    .then(dict => {
      let list = res.data.map(i => {
        return {
          _id: i._id,
          start_time: i.start_time.slice(0, -3),
          end_time: i.end_time.slice(0, -3),
          state: dict.data[0].list[i.state],
          remark: i.remark,
          showBtn: i.remark.replace(/[\u0391-\uFFE5]/g,"aa").length > 20 ? true : false,
          hideReason: i.remark.replace(/[\u0391-\uFFE5]/g,"aa").length > 20 ? true : false,
          creator: i.creator,
          creator_url: i.creator_url
        }
      })
      return {
        ...code[10000],
        data: list,
        total: res.data.length
      }
    })
  })
  .catch(err => {
    console.log('rest_list_err: ', err)
    return {
      ...code[20000]
    }
  })
}
