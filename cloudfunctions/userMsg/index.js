// 云函数入口文件
const cloud = require('wx-server-sdk')
const codeCreate = require('./codeCreate')
const code = require('./response_code')
cloud.init({
  // env: 'pageye-server-prod-7dlc5cce7ef17', // 正式环境
  env: 'pageye-server-1g4ay6gr0113942c' // 测试环境
})
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  let dbType = 'all'
  let reqData = {
    openid: wxContext.OPENID
  }
  return db.collection('users_' + dbType)
  .where(reqData)
  .get()
  .then(res => {
    switch(event.type) {
      case 'init': // 创建或更新账号
        if (res.data.length > 0) { // 更新
          let resData = res.data[0]
          Object.assign(resData, event.userInfo)
          resData.last_update_time = db.serverDate()
          if (event.role) { // 更新登录角色状态
            if (event.role === 'EMPLOYEE') { // 员工角色
              resData.log_role = event.role
            } else if (event.role === 'BOSS') { // 老板角色
              if (!resData.password) {
                resData.password = '123456'
              }
            }
          }
          return usersUpdate(resData)
        } else { // 创建
          let msg = {
            openid: reqData.openid,
            member_code: codeCreate.member(reqData.openid),
            create_time: db.serverDate(),
            last_update_time: db.serverDate(),
            wage: 11, // 默认时薪11
            log_role: event.role,
            ...event.userInfo
          }
          if (event.role === 'BOSS') { // 老板角色，增加默认密码
            msg.password = '123456'
            msg.log_role = '' // 先输入密码正确再添加角色状态
          }
          return usersCreate(msg)
        }
      case 'get': // 获取用户信息
      case 'pwd': // 老板角色密码登入
        if (res.data.length > 0) {
          if (event.type === 'get') { // 获取用户信息
            return {
              ...code[10000],
              data: res.data[0]
            }
          } else { // 老板角色，密码登入登出以及修改
            return userPwdOperation(event, res.data[0])
          }
        } else {
          return {
            ...code[40003],
            sub_msg: 'user is blank, please auth first'
          }
        }
      case 'employees': // 获取员工列表
        return {
          ...code[10000],
          data: res.data
        }
    }
  }).catch(err => {
    console.log('users_all_get_err: ', err);
    return {
      ...code[20000]
    }
  })
}

function usersCreate (reqData) {
  return db.collection('users_all')
  .add({data: reqData})
  .then(res => {
    console.log('creatres', res);
    if (res._id) { // 创建了id
      if (reqData.log_role === 'EMPLOYEE') { // 员工角色，更新员工列表
        return db.collection('users_employees')
        .add({data: reqData})
        .then(() => {
          return {
            ...code[10000],
            data: reqData
          }
        })
        .catch(err => {
          console.log('users_employees_add_err: ', err);
          return {
            ...code[20000]
          }
        })
      } else {
        return {
          ...code[10000],
          data: reqData
        }
      }
    } else {
      return {
        ...code[40004],
        sub_msg: res.errMsg
      }
    }
  }).catch(err => {
    console.log('user_list_add_err', err)
    return {
      ...code[20000]
    }
  })
}

function usersUpdate (reqData, type) {
  let id = reqData._id
  delete(reqData._id)
  return db.collection('users_all')
  .doc(id)
  .set({data: reqData})
  .then(res => {
    if (res.stats.updated === 1) {
      if (reqData.log_role === 'EMPLOYEE') { // 员工角色，更新员工列表
        return db.collection('users_employees')
        .where({openid: reqData.openid})
        .update({data: reqData})
        .then(() => {
          return {
            ...code[10000],
            data: reqData
          }
        })
        .catch(err => {
          console.log('users_employees_update_err: ', err);
          return {
            ...code[20000]
          }
        })
      } else {
        return {
          ...code[10000],
          data: reqData
        }
      }
    } else {
      return {
        ...code[40004],
        sub_msg: res.errMsg
      }
    }
  }).catch(err => {
    console.log('user_list_set_err', err)
    return {
      ...code[20000]
    }
  })
}

function userPwdOperation (reqData, resData) {
  if (reqData.sub_type === 'login') { // 增加登录角色
    // 判断密码是否正确
    if (reqData.password === resData.password) {
      resData.log_role = 'BOSS'
      return usersUpdate(resData)
    } else {
      return {
        code: 40002,
        msg: code[40002],
        sub_msg: 'password is wrong, please log again'
      }
    }
  } else if (reqData.sub_type === 'logout') { // 删除当前登录角色
    resData.log_role = ''
    return usersUpdate(resData)
  } else if (reqData.sub_type === 'edit') { // 修改登录密码
    resData[0].password = reqData.password
    return usersUpdate(resData)
  } else {
    return {
      ...code[40000]
    }
  }
}
