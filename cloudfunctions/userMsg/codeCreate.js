/**
 * 生成会员码
 * @param {String} openid openid
 * @return {Number} 会员码
 */
exports.member = function (openid) {
  let openidKey = openid.split('')
  openidKey = openidKey.map(i => i.charCodeAt())
  openidKey = openidKey.join('')
  let idLg = openidKey.toString().length
  openidKey = Math.floor(Number(openidKey)/(10**idLg)*(10**8))
  return Math.floor(openidKey + Math.random()*8)
}