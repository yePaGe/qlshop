// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'pageye-server-1g4ay6gr0113942c'
})
const db = cloud.database({
  env: 'pageye-server-1g4ay6gr0113942c'
})
let codeCreate = require('./codeCreate')

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  switch (event.type) {
    case 'init':
      return userMsgInit(event, wxContext.OPENID)
    case 'user':
    case 'boss':
    case 'employee':
      return userMsgGet(wxContext.OPENID, event.type)
    case 'money':
      return userMsgInitMoney(event, wxContext.OPENID)
    case 'logout':
      return userMsgInitLogout(event, wxContext.OPENID)

  }
}

function userMsgInit (reqData, openid) {
  if (!reqData.userInfo) {
    return {
      code: 40001,
      msg: 'missing request data'
    }
  }
  return db.collection('user-list')
  .where({openid})
  .get()
  .then(res => {
    console.log('initres', res);
    if (res.data.length > 0) { // 用户已存在，更新信息
      let msg = res.data[0]
      let id = msg._id
      Object.assign(msg, reqData.userInfo)
      msg.last_update_time = db.serverDate()
      if (reqData.role) {
        if (!msg.role) {
          msg.role = [reqData.role]
        } else {
          if (reqData.role.indexOf(msg.role) === -1) {
            msg.role.push(reqData.role)
          }
        }
        msg.log_role = reqData.role
      }
      return userMsgInitFunction(msg)
    } else { // 用户不存在，新建信息
      let msg = {
        openid: openid,
        member_code: codeCreate.member(openid),
        create_time: db.serverDate(),
        last_update_time: db.serverDate(),
        wage: 11, // 默认时薪11
        role: [reqData.role],
        log_role: reqData.role,
        ...reqData.userInfo
      }
      return db.collection('user-list')
      .add({data: msg})
      .then(res => {
        let list = 'employee-list'
        if (reqData.role === 'BOSS') list = 'boss-list'
        return db.collection(list)
        .add({data: msg})
        .then(res => {
          return {
            code: 10000,
            msg: 'success',
            data: msg
          }
        })
        .catch(err => {
          return {
            code: 20000,
            msg: 'wrong server'
          }
        })
      })
      .catch(err => {
        return {
          code: 20000,
          msg: 'wrong server'
        }
      })
    }
  })
}

function userMsgGet (openid, dbType) {
  return db.collection(dbType + '-list')
  .where({openid: openid})
  .get()
  .then(res => {
    if (res.data.length > 0) {
      if (dbType !== 'user') {
        return {
          code: 10000,
          msg: 'success',
          data: res.data
        }
      } else {
        return {
          code: 10000,
          msg: 'success',
          data: res.data[0]
        }
      }
    } else {
      return {
        code: 40001,
        msg: 'user is missing, please auth first'
      }
    }
  }).catch(err => {
    return {
      code: 20000,
      msg: 'wrong server'
    }
  })
}

function userMsgInitMoney (reqData, openid) {
  return userMsgGet(openid).then(res => {
    if (res.result.openid) {
      let money_msg = {
        year: reqData.year,
        month: reqData.month,
        money: reqData.money
      }
      res.result.money_msg = money_msg
      return userMsgInitFunction(res.result)
    } else {
      return {
        code: 40001,
        msg: 'user missing, please login again'
      }
    }
  })
}

function userMsgInitLogout (reqData, openid) {
  return userMsgGet(openid).then(res => {
    console.log('getres', res);
    if (res.data.openid) {
      res.data.log_role = ''
      return userMsgInitFunction(res.data)
    } else {
      return {
        code: 40001,
        msg: 'user missing, please login again'
      }
    }
  })
}

function userMsgInitFunction (reqData) {
  let id = reqData._id
  delete(reqData._id)
  return db.collection('user-list').doc(id).set({data: reqData}).then(sub_res => {
    console.log('initres', sub_res);
    if (sub_res.stats.updated === 1) {
      let list = 'employee-list'
      if (reqData.role === 'BOSS') list = 'boss-list'
      return db.collection(list)
        .where({openid: reqData.openid})
        .get()
        .then(res => {
          return {
            code: 10000,
            msg: 'success',
            data: reqData
          }
        })
        .catch(err => {
          return {
            code: 20000,
            msg: 'wrong server'
          }
        })
    } else {
      return {
        code: 40003,
        msg: sub_res.errMsg
      }
    }
  }).catch(err => {
    console.log('funerr', err);
    return {
      code: 20000,
      msg: 'wrong server'
    }
  })
}