module.exports = {
  "10000": {
    code: 10000,
    msg: "success"
  },
  "20000": {
    code: 20000,
    msg: "wrong server"
  },
  "40000": {
    code: 40000,
    msg: "request data is missing"
  },
  "40001": {
    code: 40001,
    msg: "request data is existed"
  },
  "40002": {
    code: 40002,
    msg: "request data is wrong"
  },
  "40003": {
    code: 40003,
    msg: "response data is blank"
  },
  "40004": {
    code: 40004,
    msg: "response data had sth wrong"
  }
}
