// 工资表云函数
const cloud = require('wx-server-sdk')
const code = require('./response_code')
const format = require('./format.js')
cloud.init({
  // env: 'pageye-server-prod-7dlc5cce7ef17', // 正式环境
  env: 'pageye-server-1g4ay6gr0113942c' // 测试环境
})
const db = cloud.database()
const _ = db.command

exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  switch(event.type) {
    case 'list': // 获取月薪列表和当月月薪
      let reqData = {
        openid: wxContext.OPENID,
        year_month_key: event.year_month
      }
      if (event.id) {
        reqData.openid = event.id
      }
      // 不需要刷新记录 init: 0
      if (event.init === 0) {
        return wageRecordList(reqData, event.wage)
      } else if (event.init === 1) { // 强制要刷新记录 init: 1
        // 获取查询年月份的打卡记录
        return signRecordList(reqData, event.wage)
      }
    case 'personal': // 修改时薪
      let reqData1 = {
        openid: wxContext.OPENID,
        wage: event.wage
      }
      return wageEdit(reqData1)
    case 'calendar': // 双计日历
      if (event.sub_type === 'get') {
        return calendarGet(event, 1)
      } else if (event.sub_type === 'save') {
        return calendarSave(event)
      }
  }
}

function wageRecordList (reqData, wage) {
  return db.collection('wage')
  .where(reqData)
  .get()
  .then(res => {
    if (res.data.length > 0) {
      return {
        ...code[10000],
        data: res.data[0].list,
        totals: res.data[0].list.length,
        total_wage: res.data[0].total_wage
      }
    } else {
      return signRecordList(reqData, wage)
    }
  })
  .catch(wage_get_err => {
    console.log('wage_get_err: ', wage_get_err)
    return {
      ...code[20000]
    }
  })
}

function signRecordList (reqData, wageKey) {
  return db.collection('signup')
  .where(reqData)
  .get()
  .then(res => {
    let signupList = res.data
    // 获取查询年月份的请假记录
    return restRecordList(reqData, rest_res => {
      // 筛选出当月请假记录
      let startNum = rest_res.start_time
      let endNum = rest_res.end_time
      let restDay = []
      let upmidDay = []
      let downmidDay = []
      // 整理出请假日期，并区别出全天假或上半天假或下半天假
      rest_res.data.forEach(i => {
        // 请假开始时间早于当月1号，结束时间早于当月尾号ok
        if (i.start_time_stamp < startNum 
          && i.end_time_stamp > startNum 
          && i.end_time_stamp < endNum) {
          let endKey = Number(i.end_time.split(' ')[0].slice(-2))
          if (new Date(i.end_time_stamp).getHours() === 12) {
            upmidDay.push(endKey)
            endKey--
          }
          for (let d = 1; d <= endKey; d++) {
            restDay.push(d)
          }
        }
        // 请假结束时间晚于当月尾号，开始时间晚于当月1号
        if (i.start_time_stamp > startNum
          && i.start_time_stamp < endNum 
          && i.end_time_stamp > endNum) {
          let startKey = new Date(i.start_time_stamp).getDate()
          if (new Date(i.start_time_stamp).getHours() === 12) {
            downmidDay.push(startKey)
            startKey++
          }
          let endKey = new Date(endNum).getDate()
          for (let d = startKey; d <= endKey; d++) {
            restDay.push(d)
          }
        }
        // 请假时间范围在当月范围内ok
        if (i.start_time_stamp > startNum
          && i.end_time_stamp < endNum) {
        let startKey = new Date(i.start_time_stamp).getDate()
          if (new Date(i.start_time_stamp).getHours() === 12) {
            downmidDay.push(startKey)
            startKey++
          }
          let endKey = new Date(i.end_time_stamp).getDate()
          if (new Date(i.end_time_stamp).getHours() === 12) {
            upmidDay.push(endKey)
            endKey--
          }
          for (let d = startKey; d <= endKey; d++) {
            restDay.push(d)
          }
        }
      })
      // 根据整理出来的日期，标记全天假日期
      if (restDay.length > 0) {
        signupList = signupList.map(i => {
          let day = Number(i.create_time.slice(-2))
          if (restDay.indexOf(day) > -1) {
            i.rest = 3 // 全天
          }
          return i
        })
      }
      // 根据整理出来的日期，标记上半天假日期
      if (upmidDay.length > 0) {
        signupList = signupList.map(i => {
          let day = Number(i.create_time.slice(-2))
          if (upmidDay.indexOf(day) > -1) {
            i.rest = 1 // 上半天
          }
          return i
        })
      }
      // 根据整理出来的日期，标记下半天假日期
      if (downmidDay.length > 0) {
        signupList = signupList.map(i => {
          let day = Number(i.create_time.slice(-2))
          if (downmidDay.indexOf(day) > -1) {
            i.rest = 2 // 下半天
          }
          return i
        })
      }
      // 计算工资
      return wageCal(signupList, wageKey, reqData)
    }, rest_err => {
      return {
        ...code[rest_err]
      }
    })
  })
  .catch(err => {
    console.log('wage_record_list_err: ', err)
    return {
      ...code[20000]
    }
  })
}

function restRecordList (reqData, success, fail) {
  let startNum = new Date(reqData.year_month_key + '-01 00:00:00').getTime()
  let endNum = 0
  let endKey = reqData.year_month_key.split('-')
  if (Number(endKey[1]) + 1 > 12) {
    endNum = new Date(Number(endKey[0])+1 + '-01-01 00:00:00').getTime() - 1000
  } else {
    endNum = new Date(endKey[0] + '-' + ((Number(endKey[1]) + 1) < 10 ? '0' + (Number(endKey[1]) + 1) : (Number(endKey[1]) + 1)) + '-01 00:00:00').getTime() - 1000
  }
  let restReqData = _.and([
    _.and({
      openid: reqData.openid
    }),
    _.or([
      { // 1、在搜索范围内
        start_time_stamp: _.and([_.gte(startNum), _.lte(endNum)]),
        end_time_stamp: _.and([_.gte(startNum), _.lte(endNum)])
      },
      { // 2、跨越开始时间，不接触结束时间
        start_time_stamp: _.lte(startNum),
        end_time_stamp: _.and([_.gte(startNum), _.lte(endNum)])
      },
      { // 3、跨越结束时间，不接触开始时间
        start_time_stamp: _.and([_.gte(startNum), _.lte(endNum)]),
        end_time_stamp: _.gte(endNum)
      },
      { // 4、跨越开始时间和结束时间
        start_time_stamp: _.lte(startNum),
        end_time_stamp: _.gte(endNum)
      }
    ])
  ])
  return db.collection('rest')
  .where(restReqData)
  .get()
  .then(res => {
    return success({
      start_time: startNum,
      end_time: endNum,
      data: res.data
    })
  })
  .catch(err => {
    console.log('rest_record_list_err: ', err)
    return fail(20000)
  })
}

function wageCal (list, wageKey, reqData) {
  let yearMonth = reqData.year_month_key.split('-')
  return calendarGet({year: yearMonth[0]})
  .then(cal => {
    let cal_list = cal.data[yearMonth[1]-1]
    let monthDate = format.monthDate(yearMonth[0], yearMonth[1], cal_list)
    let total = 0
    list = list.map(i => {
      if (cal_list.indexOf(Number(i.create_time.split('-')[2]).toString()) > -1) i.times = 2
      else i.times = 1
      i.date = i.create_time.slice(-2)
      monthDate = monthDate.filter(sub_i => sub_i.date !== i.date)
      if (i.on_time && i.off_time) {
        i.record_state = 0
        let startNumber = new Date(i.create_time + ' ' + i.on_time)
        let endNumber = new Date(i.create_time + ' ' + i.off_time)
        let offNumber = new Date(i.create_time + ' 21:00:00')
        i.on_time = i.on_time.slice(0, -3)
        i.off_time = i.off_time.slice(0, -3)
        if (i.rest === 1) { // 上午假
          startNumber = new Date(i.create_time + ' 12:00:00')
        }
        if (i.rest === 2) { // 下午假
          endNumber = new Date(i.create_time + ' 12:00:00')
        }
        i.hours = format.diff(startNumber, endNumber, 1)
        i.meal = 1
        i.lunch = 1
        if (endNumber > offNumber && i.rest !== 2) { // 九点之后下班且下午没请假，加晚餐费
          i.meal++
          i.dinner = 1
        }
        if (i.rest === 1) { // 上午请假了，没午餐费
          i.meal--
          i.lunch = 0
        }
        if (i.rest === 2 && i.dinner === 1) { // 下午请假了，没晚餐费
          i.meal--
          i.dinner = 0
        }
        i.pay = Number((i.hours * wageKey * i.times + i.meal * 10).toFixed(2))
        if (i.rest === 3) {
          i.pay = 0
        }
        total = Number((total + i.pay).toFixed(2))
      } else {
        if (!i.on_time && i.off_time) { // 上班缺卡
          i.on_time = '缺卡'
          i.off_time = i.off_time.slice(0, -3)
          i.record_state = 1
        }
        if (i.on_time && !i.off_time) { // 下班缺卡
          i.off_time = '缺卡'
          i.on_time = i.on_time.slice(0, -3)
          i.record_state = 2
        }
        if (!i.on_time && !i.off_time) { // 全天缺卡
          i.on_time = '缺卡'
          i.off_time = '缺卡'
          i.record_state = 3
        }
        i.hours = 0
        i.lunch = 0
        i.dinner = 0
        i.pay = 0
        i.meal = 0
      }
      return i
    })
    monthDate = format.wageSort(list.concat(monthDate))
    let wage = {
      ...reqData,
      total_wage: total,
      list: monthDate
    }
    return db.collection('wage')
    .where(reqData)
    .get()
    .then(res => {
      if (res.data.length > 0) { // 已有记录更新
        return db.collection('wage')
        .where(reqData)
        .update({data: wage})
        .then(update_res => {
          return {
            ...code[10000],
            data: wage.list,
            totals: wage.list.length,
            total_wage: wage.total_wage
          }
        })
        .catch(updated_wage_err => {
          console.log('updated_wage_err: ', updated_wage_err)
          return {
            ...code[20000]
          }
        })
      } else { // 未有记录新增
        return db.collection('wage')
        .add({data: wage})
        .then(add_res => {
          return {
            ...code[10000],
            data: wage.list,
            totals: wage.list.length,
            total_wage: wage.total_wage
          }
        })
        .catch(add_wage_err => {
          console.log('add_wage_err: ', add_wage_err)
          return {
            ...code[20000]
          }
        })
      }
    })
    .catch(err => {
      console.log('wage_save_err: ', err)
      return {
        ...code[20000]
      }
    })
  })
}

function wageEdit (reqData) {
  return db.collection('users_all')
  .where({openid: reqData.openid})
  .update({
    data: {
      wage: reqData.wage
    }
  })
  .then(() => {
    return {
      ...code[10000]
    }
  })
  .catch(err => {
    console.log('users_all_wage_updated_err: ', err)
    return {
      ...code[20000]
    }
  })
}

/** 
 * 日历双计
*/
function calendarGet (reqData, needAdd) {
  return db.collection('calendar')
  .where({year: reqData.year})
  .get()
  .then(res => {
    if (res.data.length > 0) {
      return {
        ...code[10000],
        data: res.data[0].list
      }
    } else {
      if (needAdd === 1) { // 需要新建传1
        db.collection('calendar')
        .add({
          data: {
            year: reqData.year,
            list: [...new Array(12)].map(i=> [])
          }
        })
      }
      return {
        ...code[10000],
        data: [...new Array(12)].map(i=> [])
      }
    }
  })
  .catch(err => {
    console.log('calendar_get_err: ', err)
    return {
      ...code[20000]
    }
  })
}

function calendarSave (reqData) {
  return db.collection('calendar')
  .where({year: reqData.year})
  .get()
  .then(res => {
    if (res.data.length > 0) {
      return db.collection('calendar')
      .where({year: reqData.year})
      .update({
        data: {
          list: reqData.list
        }
      })
      .then(update_res => {
        return {
          ...code[10000]
        }
      })
      .catch(updated_err => {
        console.log('calendar_save_updated_err: ', updated_err)
        return {
          ...code[20000]
        }
      })
    } else {
      return db.collection('calendar')
      .add({
        data: {
          year: reqData.year,
          list: reqData.list
        }
      })
      .then(add_res => {
        return {
          ...code[10000]
        }
      })
      .catch(add_err => {
        console.log('calendar_save_add_err: ', add_err)
        return {
          ...code[20000]
        }
      })
    }
  })
  .catch(err => {
    console.log('calendar_save_get_err: ', err)
    return {
      ...code[20000]
    }
  })
}
