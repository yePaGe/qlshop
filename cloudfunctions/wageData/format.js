/**
 * 计算时间差
 * @param {Time} start 开始日期时间
 * @param {Time} end 结束日期时间
 * @param {Number} type 返回类型：1，小时数；2，时间戳差
 * @return {Number} 返回类型：1，小时数；2，时间戳差
 */
exports.diff = function (start, end, type) {
  let startTime = new Date(start).getTime()
  let endTime = new Date(end).getTime()
  if (type === 1) {
    if (endTime - startTime > 0) {
      return Number(((endTime - startTime) / (1000 * 60 * 60)).toFixed(1)) // 毫秒数转为小时数
    } else {
      return 0
    }
  } else {
    return endTime - startTime
  }
}

/**
 * 数字双位处理
 * @param {Number} number 
 * @param {String} 返回 双位处理后的数字
 */
exports.fixedNumber = function (number) {
  return number.toString().length === 1 ? '0' + number.toString() : number.toString()
}

/**
 * 计算月份日期
 * @param {Number} year 年份
 * @param {Number} month 月份
 * @param {Array} doubleDate 双计日期
 * @return {Array} 返回类型：当月日期数组
 */
exports.monthDate = function (year, month, doubleDate) {
  doubleDate = doubleDate.map(i => this.fixedNumber(i))
  console.log('aaa', doubleDate)
  let bigMonth = [1, 3, 5, 7, 8, 10, 12]
  let date = [...new Array(30)].map((i, k) => this.fixedNumber(k + 1))
  if (bigMonth.indexOf(month) > -1) {
    date.push('31')
  } else if (month === '02') {
    let full = Number(year) % 4 === 0
    if (full) { // 闰年
      date.splice(-1)
    } else {
      date.splice(-2)
    }
  }
  date = date.map(i => {
    let obj = {
      date: i,
      on_time: '缺卡',
      off_time: '缺卡',
      record_state: 3,
      hours: 0,
      lunch: 0,
      dinner: 0,
      pay: 0,
      meal: 0,
      times: 1
    }
    if (doubleDate.indexOf(i) > -1) {
      console.log('dbl', i)
      obj.times = 2
    }
    return obj
  })
  return date
}

/**
 * 对象数组日期排序
 * @param {Array} wageList 工资列表
 * @return {Array} 返回类型：排序后的工资列表
 */
exports.wageSort = function (wageList) {
  wageList.sort(compare)
  return wageList
}

function compare (obj1, obj2) {
  let date1 = obj1.date
  let date2 = obj2.date
  if (Number(date1) < Number(date2)) {
    return -1
  } else if (Number(date1) > Number(date2)) {
    return 1
  } else {
    return 0
  }
}