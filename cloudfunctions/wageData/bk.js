// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'pageye-server-1g4ay6gr0113942c'
})
const db = cloud.database({
  env: 'pageye-server-1g4ay6gr0113942c'
})
const _ = db.command


// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  return {
    event,
    openid: wxContext.OPENID,
    appid: wxContext.APPID,
    unionid: wxContext.UNIONID,
  }
}